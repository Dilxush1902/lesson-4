const deadline = "2022-03-26 17:00";
function getTime(endTime) {
    const total = Date.parse(endTime)-Date.parse(new Date()),
    hour = Math.floor((total/(1000*60*60))%24),
    minutes = Math.floor((total/(1000*60))%60),
    seconds = Math.floor((total/1000)%60);
    console.log(minutes);
    return {
      total:total,
      hours:hour,
      minutes:minutes,
      seconds:seconds
    }
}

function getZero(num) {
  if(num>=0 && num<10) {
    return "0"+num;
  } else {
    return num;
  }
}
function setClock(selector,endTime) {
  const timer = document.querySelector(selector),
  hour = timer.querySelector("#hour"),
  minutes = timer.querySelector("#minutes"),
  seconds = timer.querySelector("#secondes");
  
  function upDateClock() {
    const time = getTime(endTime);
    hour.innerHTML = getZero(time.hours);
    minutes.innerHTML = getZero(time.minutes);
    seconds.innerHTML = getZero(time.seconds);
  }
  setInterval(()=>{
    upDateClock()
  },1000)
}
setClock(".main_box",deadline);
